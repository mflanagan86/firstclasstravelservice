<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */

get_header(); ?>

	<section id="error404">
			<div class="container">
				<div class="row divide">
					<div class="col-md-12" style="text-align:center">
						<h1>Oops! Page Not Found</h1>
						<h4>Appears we took a wrong turn</h4>
                        <p><strong>Note:</strong> During this current site migration, some pages may not be available. Please check back soon for updates.</p>
						<a href="/" class="m-btn blue pull-cent">Back to the Homepage</a>
					</div>
                    
                    
				</div> <!-- end row -->
            </div>    
	</section>
<?php get_footer(); ?>